class DispositivoEntrada{
    constructor(tipoEntrada, marca){
        this._tipoEntrada = tipoEntrada;
        this._marca = marca;
    }
    get getTipoEntrada(){
        return this._tipoEntrada
    }
    set setTipoEntrada(tipoEntrada){
        this._tipoEntrada = tipoEntrada;
    }
    get getMarca(){
        return this._marca
    }
    set setMarca(marca){
        this._marca = marca;
    }
}

class Raton extends DispositivoEntrada{
    static contadorRatones = 0;

    constructor(tipoEntrada, marca){
        super(tipoEntrada, marca);
        this._idRaton = ++Raton.contadorRatones;
    }
    get getIdRaton(){
        return this._idRaton
    }
    toString(){
        return `Raton: [idRaton: ${this._idRaton}, tipoEntrada: ${this._tipoEntrada}, marca: ${this._marca}]`
    }
}

class Teclado extends DispositivoEntrada{
    static contadorTeclado = 0;

    constructor(tipoEntrada, marca){
        super(tipoEntrada, marca);
        this._idTeclado = ++Teclado.contadorTeclado;
    }
    get getIdTeclado(){
        return this._idTeclado
    }
    toString(){
        return `Teclado: [idTelcado: ${this._idTeclado}, tipoEntrada: ${this._tipoEntrada}, marca: ${this._marca}]`
    }
}

class Monitor{
    static contadorMonitores = 0;

    constructor(marca, tamaño){
        this._idMonitor = ++Monitor.contadorMonitores;
        this._marca = marca;
        this._tamaño = tamaño;
    }
    get getIdMonitor(){
        return this._idMonitor
    }
    toString(){
        return `Monitor: [idMonitor: ${this._idMonitor}, marca: ${this._marca}, tamaño: ${this._tamaño}]`
    }
}

class Computadora{
    static contadorComputadoras = 0;

    constructor(nombre, monitor, raton, teclado){
        this._idComputadora = ++Computadora.contadorComputadoras;
        this._nombre = nombre;
        this._monitor = monitor;
        this._raton = raton;
        this._teclado = teclado;
    }
    toString(){
        return `Computadora ${this._idComputadora}: ${this._nombre} \n ${this._monitor} \n ${this._raton} \n ${this._teclado}`
    }
}

class Orden{
    static contadorOrdenes = 0;

    constructor(){
        this._idOrden = ++Orden.contadorOrdenes;
        this._computadoras = [];
    }
    get getIdOrden(){
        return this._idOrden
    }
    agregarComputadora(computadora){
        this._computadoras.push(computadora);
    }
    mostrarOrden(){
        let computadorasOrden = "";
        for(let computadora of this._computadoras){
            computadorasOrden += `\n${computadora}`;
        }
        console.log(`Orden: ${this._idOrden}, Computadoras: ${computadorasOrden}`);
    }
}

//Ratones / Mouse
const raton1 = new Raton("USB", "HP");
console.log(raton1.toString());
const  raton2 = new Raton("Bluetooth", "Logitech");
console.log(raton2.toString());
//Teclados
const teclado1 = new Teclado("Bluetooth", "MSI");
const teclado2 = new Teclado("USB", "MSI");
console.log(teclado1.toString());
console.log(teclado2.toString());
//Monitores
const monitor1 = new Monitor("HP", "18pg");
const monitor2 = new Monitor("Dell", "20pg");
console.log(monitor1.toString());
console.log(monitor2.toString());
//Computadoras
const computadora1 = new Computadora("HP", monitor1, raton1, teclado1);
console.log(computadora1.toString());
const computadora2 = new Computadora("Sony", monitor2, raton2, teclado2);
console.log(computadora2.toString());
//Ordenes
const orden1 = new Orden();
orden1.agregarComputadora(computadora1);
orden1.agregarComputadora(computadora2);
orden1.mostrarOrden();