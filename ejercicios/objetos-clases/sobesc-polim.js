class Empleado{
    constructor(nombre, sueldo){
        this._nombre = nombre;
        this._sueldo = sueldo;
    }
    obtenerDetalles(){
        return `Empleado: ${this._nombre}, ${this._sueldo}`
    }
}

class Gerente extends Empleado{
    constructor(nombre, sueldo, departamento){
        super(nombre, sueldo);
        this._departamento = departamento;
    }
    // sobreescritura
    obtenerDetalles(){
        return `Gerente: ${super.obtenerDetalles()}, depto: ${this._departamento}`
    }
}

// polimorfismo --> utiliza un metodo para que mas de un objeto pueda hacer uso de una funcionalidad
function imprimir(tipo){
    if(tipo instanceof Empleado){
        console.log(tipo.obtenerDetalles());
        console.log(`Es un tipo Empleado`);
    }
    else if(tipo instanceof Gerente){
        console.log(tipo.obtenerDetalles());
        console.log(`Es un tipo Gerente`);
    }
    else if(tipo instanceof Object){
        console.log(`Es un tipo Object`);
    }
}

const empleado1 = new Empleado("Marcos", "$100000");
const gerente1 = new Gerente("Roberto", "$120000", "Sistemas");
const celular1 = {
    marca: "Nokia",
    modelo: "5.1 Plus",
    año: "2019"
}

imprimir(empleado1);
imprimir(gerente1);
imprimir(celular1);