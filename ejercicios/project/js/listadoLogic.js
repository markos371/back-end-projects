const Personas = [
    new Persona("Marcos", "Hinca"),
    new Persona("Matheo", "Hinca"),
    new Persona("Silvia", "Bais")
];

function mostrarPersonas(){
    let texto = "";
    for(let persona of Personas){
        console.log(persona);
        texto += `<li>${persona._nombre} ${persona._apellido}</li>`
    }
    document.getElementById("personas").innerHTML = texto;
}

function agregarPersonas(){
    const forma = document.forms["forma"];
    const nombre = forma["nombre"];
    const apellido = forma["apellido"];
    if(nombre.value !== "" && apellido.value !== ""){
        const persona = new Persona(nombre.value, apellido.value);
        console.log(persona);
        Personas.push(persona);
        mostrarPersonas();
    }
    else{
        console.log("No hay informacion que ingresar")
    }
}