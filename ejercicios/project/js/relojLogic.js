const mostrarReloj = () =>{
    const fecha = new Date();
    const hr = formatoHora(fecha.getHours());
    const min = formatoHora(fecha.getMinutes());
    const sec = formatoHora(fecha.getSeconds());
    document.getElementById("hora").innerHTML = `${hr}:${min}:${sec}`;

    const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    const dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    const diaSemana = dias[fecha.getDay()];
    const dia = fecha.getDate();
    const mes = meses[fecha.getMonth()];
    const anio = fecha.getFullYear();
    const fechaTexto = `${diaSemana}, ${dia} ${mes} ${anio}`;
    document.getElementById("fecha").innerHTML = fechaTexto;

    document.getElementById("contenedor").classList.toggle("animar");
}

const formatoHora = hora =>{
    if(hora < 10)
        hora = "0" + hora;
    return hora
}

setInterval(mostrarReloj, 1000);