class Persona{
    static contadorPersona = 0;

    constructor(nombre, apellido){
        this._nombre = nombre;
        this._apellido = apellido;
        this._idPersona = ++Persona.contadorPersona;
    }
    get getName(){
        return this._nombre
    }
    set setName(nombre){
        this._nombre = nombre;
    }
    get getApellido(){
        return this._apellido
    }
    set setApellido(apellido){
        this._apellido = apellido;
    }
    get getIdPersona(){
        return this._idPersona
    }
}