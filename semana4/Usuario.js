class Usuario{
    constructor(nombre, apellido, email, pais, clave, claveRepetida){
        this._nombre = nombre;
        this._apellido = apellido;
        this._email = email;
        this._pais = pais;
        this._clave = clave;
        this._claveRepetida = claveRepetida;
    }
}