require("dotenv").config();
const express = require("express");
const app = express();

app.use(express.json());

const datos = [
    {marca: "Nokia", gama: "media", modelo: "5.1 plus", pantalla: " 5.86, 720 x 1520 pixels",
    sistemaOperativo: "Android 10", Precio: "$18000"},
    {marca: "Motorola", gama: "media", modelo: "G9 play", pantalla: "LCD 6,5 pulgadasResolución HD+",
    sistemaOperativo: "Android 11", Precio: "$25000"},
    {marca: "Nokia", gama:" media", modelo: "7.1 plus", pantalla: " 6.18, 1080 x 2280 pixels",
    sistemaOperativo: "Android 11", Precio: "$24000"}
]


app.get("/celulares", (req, res) => res.send(datos));
app.get("/celularesMitad", (req, res) => {
    let aux = Math.round(datos.length / 2);
    let arrAux = datos.splice(0, aux);
    res.send(arrAux);
});
app.get("/celularMasBarato", (req, res) => {
    let barato = datos[0];
    datos.forEach(elem => {
        if(elem.precio < barato.Precio) barato = elem;
    });
    res.send(barato);
});
app.get("/celularMasCaro", (req,res) => {
    let caro = datos[0];
    datos.forEach(elem => {
        if(elem.precio > caro.Precio) caro = elem;
    });
    res.send(caro);
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Escuchando en el puerto ${port}`);
});