const express = require("express");
const app = express();
const biblioteca = require("./data");

const verificarExistencia = id => biblioteca.find(elem => elem.id === parseInt(id));
const verificarExistenciaLibro = (libros, id) => libros.find(elem => elem.id === parseInt(id)); 

app.use(express.json());

app.get("/autores", (req, res) => {
    res.send(biblioteca);
});

app.get("/autores/:id", (req, res) => {
    let autor = verificarExistencia(req.params.id);
    if(!autor) res.status(404).send("Autor no encontrado!");
    res.send(autor);

});

app.post("/autores", (req, res) => {
    const aux = req.body;
    let autor = {
        id: biblioteca.length + 1,
        nombre: aux.nombre,
        apellido: aux.apellido,
        libros: aux.libros
    }
    biblioteca.push(autor);
    res.status(200).send(autor);
});

app.put("/autores/:id", (req, res) => {
    let autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Auto no encontrado!");
        return
    }
    const nuevoAutor = req.body;
    const id = autor.id;
    autor = nuevoAutor;
    autor.id = id;
    console.log(autor.nombre);
    res.send(autor);
});

app.delete("/autores/:id", (req, res) => {
    const autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Autor no encontrado!");
        return
    }        
    const index = biblioteca.indexOf(autor);
    biblioteca.splice(index,1);
    res.send(biblioteca);
});

app.get("/autores/:id/libros", (req, res) => {
    let autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Autor no encontrado!");
        return  
    } 
    res.send(autor.libros);
});

app.post("/autores/:id/libros", (req, res) => {
    let autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Autor no encontrado!");
        return
    }
    const aux = req.body;
    const  nuevoLibro = {
        id: autor.libros.length + 1,
        titulo: aux.titulo,
        descripcion: aux.descripcion,
        anioPublicacion: aux.anioPublicacion
    };
    autor.libros.push(nuevoLibro);
    res.send(nuevoLibro);
});

app.get("/autores/:id/libros/:idLibros", (req, res) => {
    const autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Autor no encontrado!");
        return
    }
    const idLibro = verificarExistenciaLibro(autor.libros, req.params.idLibros);
    if(!idLibro){
        res.status(404).send("Libro no encontrado!");
        return
    }
    res.send(idLibro);
});

app.put("/autores/:id/libros/:idLibros", (req, res) => {
    const autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Autor no encontrado!");
        return
    }
    let idLibro = verificarExistenciaLibro(autor.libros, req.params.idLibros);
    if(!idLibro){
        res.status(404).send("Libro no encontrado!");
        return
    }
    const nuevoLibro = req.body;
    const id = autor.libros.id;
    idLibro = nuevoLibro
    idLibro.id = id;
    console.log(idLibro);
    res.send(idLibro);
});

app.delete("/autores/:id/libros/:idlibros", (req, res) => {
    const autor = verificarExistencia(req.params.id);
    if(!autor){
        res.status(404).send("Autor no encontrado!");
        return
    }
    const idLibro = verificarExistenciaLibro(autor.libros, req.params.idlibros);
    if(!idLibro){
        res.status(404).send("Libro no encontrado!");
        return
    }
    const index = autor.libros.indexOf(idLibro);
    autor.libros.splice(index, 1);
    res.send(autor.libros);
});

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`Escuchando en el puerto ${port}`);
});
