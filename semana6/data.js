const libros = [
    {id: 1,
    nombre: "Jorge Luis",
    apellido: "Borges",
    libros: [
        {id: 1,
        titulo: "Ficciones",
        descripcion: "Se trata de uno de sus mas...",
        anioPublicacion: 1944},
        {id: 2,
        titulo: "El Aleph",
        descripcion: "Otra recopilacion de cuentos...",
        anioPublicacion: 1949}
    ]},
    {id: 2,
    nombre: "Rodolfo",
    apellido: "Walsh",
    libros: [
        {id: 1,
        titulo: "Operacion Masacre",
        descripcion: "Hay un fusilado que vive...",
        anioPublicacion: 2018},
        {id: 2,
        titulo: "Los irlandeses",
        descripcion: "El ciclo de los irlandeses...",
        anioPublicacion: 2011}
    ]}
]

module.exports = libros