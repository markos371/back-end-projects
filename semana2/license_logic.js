document.getElementById("formulario-licencia").addEventListener("click",function() {
    const nombre = document.getElementById("nombre").value;
    const apellido = document.getElementById("apellido").value;
    const edad = document.getElementById("edad").value;
    const licencia = document.getElementById("licencia").value;
    const fecha_expiracion = document.getElementById("fecha_expiracion").value;
    if (edad > 18 && licencia === "si" && parseInt(fecha_expiracion) < 20210708) {
        console.log(nombre + " " + apellido + " esta habilitado para conducir"); 
    }
    else {
        console.log(nombre + " " + apellido + " no esta habilitado para conducir")
    }
})

const cabecero = document.getElementById("cabecero");  // guarda todo el objeto de h1 en cabecero
cabecero.innerHTML = "Licencia de conducir";  // intercambia el valor anterior por el valor de "licencia de conducir"

const numInput = document.getElementsByTagName("input");  // guarda una coleccion de objetos de un tipo de etiqueta HTML, es el "<input>"
console.log(`Numero de inputs: ${numInput.length}`);  // muestra por pantalla el numero de elementos de la coleccion

const numLabel = document.getElementsByTagName("label"); // guarda una coleccion de objetos de un tipo de etiqueta HTML, es el "<label>"
for(let i=0; i<numLabel.length; i++) {
    console.log(`${i} - ${numLabel[i].innerHTML}`);  // muestra por pantalla el valor de los elementos de la collecion 
}

const elementos = document.getElementsByClassName("opcion");
console.log(`Numero de elementos con la clase: ${elementos.length}`);
for(let elemento of elementos) {
    console.log(`Opcion: ${elemento.innerHTML}`)
}

const elemSelected = document.querySelectorAll("h1.cabecero");
console.log(`Numero de elementos con la etiqueta h1 y la clase cabecero: ${elemSelected.length}`);
for(let elemento of elemSelected) {
    console.log(`Opcion: ${elemento.innerHTML}`);
}