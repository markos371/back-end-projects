document.getElementById("btn-sumar").addEventListener("click", function() {
    const numero1 = document.getElementById("num1").value;
    const numero2 = document.getElementById("num2").value;
    const resultado = parseInt(numero1) + parseInt(numero2);
    console.log("Resultado suma: ", resultado);
    document.getElementById("resultado").innerHTML = "El resultado de la suma es " + resultado;
})

document.getElementById("btn-restar").addEventListener("click", function() {
    const numero1 = document.getElementById("num1").value;
    const numero2 = document.getElementById("num2").value;
    const resultado = parseInt(numero1) - parseInt(numero2);
    console.log("Resultado resta: ", resultado);
    document.getElementById("resultado").innerHTML = "El resultado de la resta es " + resultado;
})

document.getElementById("btn-multiplicar").addEventListener("click", function() {
    const numero1 = document.getElementById("num1").value;
    const numero2 = document.getElementById("num2").value;
    const resultado = parseInt(numero1) * parseInt(numero2);
    console.log("Resultado multiplicación: ", resultado);
    document.getElementById("resultado").innerHTML = "El resultado de la multiplicación es " + resultado;
})

document.getElementById("btn-dividir").addEventListener("click", function() {
    const numero1 = document.getElementById("num1").value;
    const numero2 = document.getElementById("num2").value;
    const resultado = parseInt(numero1) / parseInt(numero2);
    console.log("Resultado división: ", resultado);
    document.getElementById("resultado").innerHTML = "El resultado de la división es " + resultado;
})

document.getElementById("btn-cuadrado").addEventListener("click", function() {
    const numero1 = document.getElementById("num1").value;
    const numero2 = document.getElementById("num2").value;
    const quad = 2;
    const resultado = parseInt(numero1) ** quad + parseInt(numero2) ** quad;
    console.log("Resultado cuadrados:", resultado);
    document.getElementById("resultado").innerHTML = "El resultado de la suma de los cuadrados es " + resultado;
})



