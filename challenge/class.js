class Estudiante{
    static numEstudiante = 0;

    constructor(nombre, apellido, edad, ciudad){
        this._idEstudiante = ++Estudiante.numEstudiante;
        this._nombre = nombre;
        this._apellido = apellido;
        this._edad = edad;
        this._ciudad = ciudad;
    }
    get getIdEstudiante(){
        return this._idEstudiante
    }
    get getNombre(){
        return this._nombre
    }
    set setNombre(nombre){
        this._nombre = nombre;
    }
    get getApellido(){
        return this._apellido
    }
    set setApellido(apellido){
        this._apellido = apellido;
    }
    get getEdad(){
        return this._edad
    }
    set setEdad(edad){
        this._edad = edad;
    }
    get getCiudad(){
        return this._ciudad
    }
    set setCiudad(ciudad){
        this._ciudad = ciudad;
    }
    toString(){
        return `Estudiante ${this._idEstudiante}: ${this._nombre} ${this._apellido}, ${this._edad} años, ${this._ciudad}, Tierra del fuego`
    }
}

const estudiante1 = new Estudiante("Marcos", "Hinca", 19, "Ushuaia");
console.log(estudiante1.getCiudad);
console.log(estudiante1.toString());
const estudiante2 = new Estudiante("Jorge", "Salvador", 22, "Rio Grande");
console.log(estudiante2.getEdad);
console.log(estudiante2.toString());
const estudiante3 = new Estudiante("Jana", "Flores", 20, "Ushuaia");
console.log(estudiante3.getApellido);
console.log(estudiante3.toString());
const estudiante4 = new Estudiante("Julian", "Lopez", 21, "Tolhuin");
console.log(estudiante4.getNombre);
console.log(estudiante4.toString());