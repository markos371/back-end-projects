const  condicionUno = (1 == "1");
const  condicionDos = (1 === "1");
const condicionTres = (0 || 1);
const condicionCuatro = (true && 1);
const condicionCinco = (1-1) && (2+2);
const condicionSeis = (1-1) || (2+2);

console.log(condicionUno);
console.log(condicionDos);
console.log(condicionTres);
console.log(condicionCuatro);
console.log(condicionCinco);
console.log(condicionSeis);
