require("dotenv").config();
const express = require("express");
const cors = require("cors");
const app = express();
const users = require("./routes/users");
const products = require("./routes/products");
const orders = require("./routes/orders");
const signUp = require("./routes/sign-up");
const login = require("./routes/login");
const paymentMethods = require("./routes/paymentMethods");

app.use(cors());

app.use(express.json());

app.use("/users", users);
app.use("/products", products);
app.use("/orders", orders);
app.use("/sign-up", signUp);
app.use("/login", login);
app.use("/paymentMethods", paymentMethods);

app.use((err, req, res, next) => {
    res.status(400).json({message: err.message});
});
const port = process.env.NODE_PORT || 5000;

app.listen(port, () => {
    console.log(`Escuchando en el puerto ${port}`);
});