const users = require("../models/users");

const emailDupicado = (users, email) => {
    const prueba = users.some(elem => elem.email === email);
    return prueba
};

const getUser = (data, user, pasw) => { 
    const object = data.find(elem => elem.userName === user || elem.email === user && elem.password === pasw);
    return object
};

const verifyExistanceUser = (users, user, pasw) => {
    for(let elem of users){
        if(elem.userName === user || elem.email === user && elem.password === pasw)
            return true 
    }
    return false
}; 

const validate = validation => {
    return (req, res, next) => {
        try {
            validation(req.body);
            next();
        } catch(error) {
            next(error);
        }
    }
};

const user_validated = data => {
    console.log("Paso por aqui {validacion de inicio de sesion}");
    const {userOrEmail, password} = data;
    console.log(userOrEmail);
    console.log(password);
    if(!/^[a-z0-9_.]+$/i.test(userOrEmail)){
        if(!/^[a-z0-9_.]+@[a-z0-9]+\.[a-z0-9_.]+$/i.test(userOrEmail)){
            throw new Error("Error de inicio de sesion. Usuario incorrecto");
        }
    }

    if(password.length > 128 || password.length < 8){
        throw new Error("Error de inicio de sesion. Contrasena incorrecta");
    }

    if(!verifyExistanceUser(users, userOrEmail, password)){
        throw new Error("Error de inicio de sesion. No existe este usuario registrado con esas credenciales")
    };
};

// aca valido si el registro cumple con los requerimientos
const newUser_validated = (data) => {
    console.log("Paso por aqui {validacion de un nuevo usuario}");
    const {userName, name_lastName, email, phoneNumber, shippingAddress, password} = data;
    console.log(userName);
    console.log(name_lastName);
    console.log(email);
    console.log(phoneNumber);
    console.log(shippingAddress);
    console.log(password);

    // se permite ingresar una cadena de texto y numeros pero NO solo numeros 
    if(!/^[a-z0-9_.]+$/i.test(userName) || /^[0-9]+$/i.test(userName)) {
        throw new Error("Usuario debe solamente contener caracteres.");
    }

    if(userName.length <= 2 || userName.length > 32){
        throw new Error("Usuario debe contener minimo 3 caracteres y como maximo 32 caracteres");
    }

    if(!/^[a-z ]+$/i.test(name_lastName)){
        throw new Error("Nombre y apellido deben solamente contener caracteres");
    }

    if(typeof email !== "string"){
        throw new Error("El email ingresado debe ser una cadena de caracteres");
    }

    if(!/^[a-z0-9_.]+@[a-z0-9]+\.[a-z0-9_.]+$/i.test(email)){
        throw new Error("El email debe tener formato de una cuenta de email");
    }

    if(emailDupicado(users, email)){
        console.log("Entro a este bloque")
        throw new Error("Ya existe una cuenta registrada con ese email, utilice otro");
    }

    if(!/^[+0-9 ]+$/i.test(phoneNumber)){
        throw new Error("Telefono solamente debe contener numeros")
    }

    if(password.length > 128 || password.length < 8){
        throw new Error("Contrasena debe tener minimo 8 caracteres");
    }
};

// valido si existe en usuario mediante su id
const isRegistered = data => {
    console.log("Paso por aqui {validacion de si esta registrado}");
    let userId = parseInt(data.id);
    if(!/^[0-9]+$/i.test(userId) || userId === 0){
        throw new Error("Error. Debe ingresar un valor numerico valido como id del usuario");
    }
    userId--;
    if(users.length > userId){
        if(users[userId].isAdmin){
            throw new Error("Error, ID incorrecto. No puedes realizar un pedido");
        } else {
            console.log("Ese id es de un usuario");
        }
    } else 
        throw new Error("No existe un usuario con ese id.");
        
};

module.exports = {
    validate,
    newUser_validated,
    user_validated,
    verifyExistanceUser,
    getUser,
    isRegistered
};