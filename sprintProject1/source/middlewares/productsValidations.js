const products = require("../models/products");


// FALTA VALIDAR PRODUCTO, para darlo de alta en la lista de productos
const product_validated = data => {
    console.log("Paso por aqui {validacion de productos}");
    const {name, description, price} = data;
    console.log(name);
    console.log(description);
    console.log(price);
    if(!/^[a-z ]+$/i.test(name)){
        throw new Error("Error. El nombre del producto debe ser una cadena de caracteres");
    }
    if(!/^[a-z ]+$/i.test(description)){
        throw new Error("Error. La descripcion del producto debe ser una cadena de caracteres");
    }
    if(typeof price !== "number"){
        throw new Error("Error. El precio debe ser un valor numerico");
    }
};



module.exports = {
    product_validated
}