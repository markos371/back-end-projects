const paymentMethods = require("../models/paymentMethods");

const medioDePagoDupicado = (paymentMethods, paymentName) => {
    const resp = paymentMethods.some(elem => elem.paymentName === paymentName);
    return resp
};

const paymentMethod_validated = data => {
    console.log("Paso por aqui {validacion de metodos de pago}");
    const {paymentName} = data;
    console.log(paymentName);
    if(!/^[a-z ]+$/i.test(paymentName)){
        throw new Error("El metodo de pago es incorrecto");
    }
    if(medioDePagoDupicado(paymentMethods,paymentName))
        throw new Error("Error, Medio de pago ya existente");
};

module.exports = {
    paymentMethod_validated
}