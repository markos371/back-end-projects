const users = require("../models/users");

const validateBodyInputs = validation => {
    return (req, res, next) => {
        try {
            validation(req.body);
            next();
        } catch(error) {
            next(error);
        }
    }
};

const validateHeadersInputs = validation => {
    return (req, res, next) => {
        try {
            validation(req.headers);
            next();
        } catch(error) {
            next(error);
        }
    }
}

const validateParamsInputs = validation => {
    return (req, res, next) => {
        try {
            validation(req.params);
            next();
        } catch(error) {
            next(error);
        }
    }
}

const isAdmin = data => {
    console.log("Paso por aqui {validacion de administrador}");
    let userId = parseInt(data.userid);
    if(!/^[0-9]+$/i.test(userId) || userId === 0){
        throw new Error("Error. Debe ingresar un valor numerico valido como id del usuario");
    }
    userId--;
    if(users.length > userId){
        if(!users[userId].isAdmin){
            throw new Error("Error, ID incorrecto. No tienes acceso al las funcionalidades de un administrador");
        } else {
            console.log("Ese id es el del administrador");
        }
    } else 
        throw new Error("No existe un usuario con ese id.");
    
};

// CAMBIAR PARA QUE SE PUEDE INGRESAR EL 0 ej --> 10, 20, 100
const isNumberId = data => {
    console.log("Paso por aqui {validacion de parametros ingresados}");
    const id = parseInt(data.id);
    if(!/^[0-9]+$/i.test(id) || id === 0){
        throw new Error("Error. Debe ingresar un valor numerico valido como id del producto");
    }
};

// aca valido si el indice es valido
// const product_index_validated = (req, res, next) => {
//     console.log("paso por aqui");
//     next();
// };

module.exports = {
    validateHeadersInputs,
    isAdmin,
    validateParamsInputs,
    isNumberId,
    validateBodyInputs
}