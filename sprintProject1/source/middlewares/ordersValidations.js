const orders = require("../models/orders");
const status = ["","Pendiente", "Confirmado", "En preparacion", "Enviado", "Entregado"];
const paymentMethods = require("../models/paymentMethods");
const products = require("../models/products");


// FALTA VERIFICAR SI CADA COMIDA DENTRO DE DATA SE ENCUENTRA EN EL MENU
const verifyMenu = data => {
    let correctName = false;
    let correctOrders = 0;
    let i = 0;
    for(let elem of data) {
        while(!correctName && i < products.length){
            if(elem === products[i].name){
                correctName = true;               
                correctOrders++;
            }
            i++;
            if(correctName)
                i = 0;
        }
        correctName = false;
    }
    if(correctOrders === data.length)
        return correctOrders
    else 
        return false
};

const orderStatus_validated = data => {
    console.log("Paso por aqui {validacion de ordenes para cambio de estado}");
    const {newStatus} = data;
    console.log(newStatus);
    if(!(newStatus in status))
        throw new Error("Error. Ese estado no existe");
};

// en la validacion tengo que dividir la cadena separado pro comas, para obtener las comidas por separado
const order_validated = data => {
    console.log("Paso por aqui {validacion de ordenes de clientes}");
    let {dishName, quantityProducts, paymentMethod, shippingAddress} = data;
    console.log(dishName);
    console.log(quantityProducts);
    console.log(paymentMethod);
    console.log(shippingAddress);
    dishName = dishName.split(",");
    console.log(dishName);
    quantityProducts = quantityProducts.split(",");
    console.log(quantityProducts);
    let noNumber = false; // para comprobar si todos los elementos de un array son numeros
    let i = 0;
    if(!verifyMenu(dishName)){
        throw new Error("Error. Hay productos que no se encuentran en el menu");
    }

    if(dishName.length !== quantityProducts.length){
        throw new Error("Error. Debe seleccionar una cantidad de productos para cada plato");
    }

    while(!noNumber && i < quantityProducts.length){
        if(!/^[0-9]+$/i.test(quantityProducts[i] || quantityProducts[i] === 0)){
            noNumber = true;
        }
        i++;
    }
    if(noNumber)
        throw new Error("Error. La cantidad de productos debe ser un numero valido");

    const indexPaymentMethod = paymentMethods.findIndex(elem => elem.paymentName === paymentMethod)
    if(indexPaymentMethod === -1){
        throw new Error("Error. Metodo de pago incorrecto");
    }
};

const checkPendingStatus = (userId) => {
    for(let order of orders){
        if(order.user.id === userId && order.status === "Pendiente")
            return true
    }
    return false
};

const calculateTotal = (productsArr, quantityProductsArr) => {
    console.log(productsArr);
    console.log(quantityProductsArr);
    let total = 0;
    let i = 0;
    let j = 0;
    let calculated = false;
    for(let elem of productsArr){
        while(!calculated && i < products.length){
            if(elem === products[i].name){
                calculated = true;   
                total += products[i].price * parseInt(quantityProductsArr[j]);            
                j++;
            }
            i++;
            if(calculated)
                i = 0;
        }
        calculated = false;
    }
    return total
};

const firstOrder = () => {

};

// aca valido si el indice es valido
// const product_index_validated = (req, res, next) => {
//     console.log("paso por aqui");
//     next();
// };

module.exports = {
    orderStatus_validated,
    order_validated,
    checkPendingStatus,
    calculateTotal,
    firstOrder,
    status
}