const orders = [
    {
        "id": 1,
        "detail": ["Sopa miso", "Verduras al wok"],
        "quantityProducts": [3, 2],
        "total": 475,
        "status": "Pendiente",
        "paymentMethod": "Efectivo",
        "user": {
            "id": 3,
            "name_lastName": "Logan Jerry",
            "userName": "LogsJerry",
            "email": "ljerry@mail.com",
            "phoneNumber": 213142352,
            "address": "1 Denis PIKensignton, London W8 6DE, UK",
        }
    }
];

module.exports = orders;