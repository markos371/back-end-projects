const users = [
    {
        "id": 1,
        "userName": "markc1234",
        "name_lastName": "Marcos Hinca",
        "email": "markos@mail.com",
        "phoneNumber": 134325351,
        "shippingAddress": "belgrano 557",
        "password": "MARCOS3107",
        "isAdmin": true,
        "isLogged": true,
        "hasOrdered": false
    },
    {
        "id": 2,
        "userName": "NotDark",
        "name_lastName": "Marco Kozo",
        "email": "marcos.fabricio@gmail.com",
        "phoneNumber": 533244423,
        "shippingAddress": "Crisologo Larralde 2731",
        "password": "MARKOZO31",
        "isAdmin": false,
        "isLogged": false,
        "hasOrdered": false
    },
    {
        "id": 3,
        "userName": "LogsJerry",
        "name_lastName": "Logan Jerry",
        "email": "ljerry@mail.com",
        "phoneNumber": "213142352",
        "shippingAddress": "1 Denis PIKensignton, London W8 6DE, UK",
        "password": "LOGAN5789",
        "isAdmin": false,
        "isLogged": false,
        "hasOrdered": true
    }
];

module.exports = users;