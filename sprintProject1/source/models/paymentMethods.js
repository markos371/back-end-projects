const paymentMethods = [
    {
        "id": 1,
        "paymentName": "Efectivo"
    }, 
    {
        "id": 2,
        "paymentName": "Transferencia bancaria"
    }, 
    {
        "id": 3,
        "paymentName": "Tarjeta de credito"
    }, 
    {
        "id": 4,
        "paymentName": "Tarjeta de debito"
    }, 
    {
        "id": 5,
        "paymentName": "PayPal"
    }
];

module.exports = paymentMethods;