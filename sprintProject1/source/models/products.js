const  products = [
    {
        "name": "Salmon hono yuzu",
        "description": "Salmón (15g) flameado concebollín, poro frito, ajodorado y salsa yuzu",
        "price": 75,
        "id": 1
    },
    {
        "name": "Sopa miso",
        "description": "Caldo de dashicon cebollín, harusame y tofu",
        "price": 55,
        "id": 2
    },
    {
        "name": "Camarones Rocca",
        "description": "Camarones crujientes (100g) con aderezo rocca a base de sriracha",
        "price": 145,
        "id": 3
    },
    {
        "name": "Verduras al wok",
        "description": `Kale, edamames, espárragos, brócoli, camote, calabaza, zanahoria, chícharos, granos de elote y portobello al wok sobre arroz integral con semillas de girasol`,
        "price": 155,
        "id": 4
    }
];

module.exports = products;