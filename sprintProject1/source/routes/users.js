const express = require("express");
const router = express.Router();
const users = require("../models/users");
const usersValidations = require("../middlewares/usersValidations");
const inputsValidations = require("../middlewares/inputsValidations");

router.get("/get", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), (req, res) => {
    res.status(200).json({message: users});
});

router.post("/add", (req, res) => {
    res.send("Adding new data...");
});
router.put("/update", (req, res) => {
    res.send("Updating users data...");
});
router.delete("/delete", (req, res) => {
    res.send("Deleting users data...");
});

module.exports = router;