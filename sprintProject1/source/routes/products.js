const express = require("express");
const router = express.Router();
const products = require("../models/products");
const productsValidations = require("../middlewares/productsValidations");
const inputsValidations = require("../middlewares/inputsValidations")

const balanceProductIndex = (data)=> {
    let productId = parseInt(data.id);
    let productsIndex = productId - 1
    console.log("desde la funcion balance");
    console.log(productId);
    console.log(productsIndex);
    do{
        console.log(products);
        console.log(products[productsIndex]);
        products[productsIndex].id = productId++;  
        console.log(products[productsIndex]);   
        productsIndex++; 
    }while(productsIndex < products.length)
};

router.get("/get", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), (req, res) => {
    res.json({message: products});
});

router.post("/add", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), 
                inputsValidations.validateBodyInputs(productsValidations.product_validated), (req, res) => {
    let {name, description, price} = req.body;
    const id = products.length + 1;
    products.push({name, description, price, id});
    res.json({message: "Producto dado de alta"});
});

// PARA PUT Y DELETE FALTA PODER PERMITIR MAS DE UNA PALABRA CUANDO SE INGRESA MAS DE UNA PALABRA EN EL NOMBRE
// Y APELLIDO DE LO INGRESADO EN EL BODY

router.put("/update/:id", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), 
                   inputsValidations.validateBodyInputs(productsValidations.product_validated), (req, res) => {
    const productId = req.params.id;
    const productToUpdate = products.find(elem => elem.id === parseInt(productId));
    if(!productToUpdate){
        throw new Error("No existe un producto con ese id");
    }
    const {name, description, price} = req.body;
    productToUpdate.name = name;
    productToUpdate.description = description;
    productToUpdate.price = price;
    res.status(200).json({message: `Producto actualizado`});
});

router.delete("/delete/:id", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), 
                      inputsValidations.validateParamsInputs(inputsValidations.isNumberId), (req, res) => {
    const productId = req.params.id;
    const productToDeleteIndex = products.findIndex(elem => elem.id === parseInt(productId));
    console.log(productId);
    console.log(productToDeleteIndex);
    if(productToDeleteIndex === -1){
        throw new Error("No existe un producto con ese id");
    }
    products.splice(productToDeleteIndex, 1);
    if(productToDeleteIndex !== products.length)
        balanceProductIndex(req.params);
    res.status(200).json({message:`Producto eliminado`});
});

module.exports = router;