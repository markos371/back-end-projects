const express = require("express");
const router = express.Router();
const orders = require("../models/orders");
const users = require("../models/users");
const ordersValidations = require("../middlewares/ordersValidations");
const inputsValidations = require("../middlewares/inputsValidations");
const usersValidations = require("../middlewares/usersValidations");

router.get("/getOrderAdmin", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), (req, res) => {
    res.json({message: orders});
});

router.get("/getOrdersUser/:id", inputsValidations.validateParamsInputs(usersValidations.isRegistered), (req, res) => {
    const userId = parseInt(req.params.id);
    allOrders = [];
    for(let order of orders){
        if(order.user.id === userId)
            allOrders.push({order});
    }
    res.json({message: allOrders});
});

// QUE LOS USUARIOS PUEDEN ELEGIR "MEDIOS" DE PAGO, supongo que mas de uno
router.post("/newOrderUser/:id", inputsValidations.validateParamsInputs(usersValidations.isRegistered)
                            , inputsValidations.validateBodyInputs(ordersValidations.order_validated), (req, res) => {
    const userId = parseInt(req.params.id);
    if(ordersValidations.checkPendingStatus(userId)){
        throw new Error("Error, usted tiene una orden en estado pendiente, confirme su otro pedido para realizar otro");
    }
    const {dishName, paymentMethod, shippingAddress}  = req.body;
    let quantityProducts = req.body.quantityProducts;
    const detail = dishName.split(",");
    quantityProducts = quantityProducts.split(",");
    const total = ordersValidations.calculateTotal(detail, quantityProducts);
    const address = shippingAddress;
    const id = orders.length + 1;
    const status = "Pendiente";
    const user = {
        "id": userId,
        "name_lastName": users[userId - 1].name_lastName,
        "userName": users[userId - 1].userName,
        "email": users[userId - 1].email,
        "phoneNumber": users[userId - 1].phoneNumber,
        "address": address
    }
    users[userId - 1].hasOrdered = true;
    orders.push({id, detail, quantityProducts, total, status, paymentMethod, user});
    res.json({message: "Orden relizada con exito"});
});

router.put("/changeStatus/:id", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin)
                           , inputsValidations.validateParamsInputs(inputsValidations.isNumberId)
                           , inputsValidations.validateBodyInputs(ordersValidations.orderStatus_validated),(req, res) => {
    const orderId = parseInt(req.params.id);
    console.log(orders);
    const orderStatusToUpdate = orders.find(elem => elem.id === orderId);
    console.log(orderStatusToUpdate);
    if(!orderStatusToUpdate){
        throw new Error("No existe una orden con ese id");
    }
    const {newStatus} = req.body;
    orderStatusToUpdate.status = ordersValidations.status[newStatus];
    res.json({message: `Estado modificado`});
});

// SE INTERCAMBIA EL DIRECTAMENTE EL DETALLE DEL PEDIDO ANTERIOR POR EL LOS NUEVOS PEDIDOS (PLATOS)
// QUE LOS USUARIOS "PUEDAN" AGREGAR UNA DIRECCION A SU PEDIDO, SUPONGO QUE NO SOLO INTERAMBIAR LA DIRECCION DEL PEDIDO
router.put("/modifyOrderUser/:id", inputsValidations.validateParamsInputs(usersValidations.isRegistered)
                            , inputsValidations.validateBodyInputs(ordersValidations.order_validated), (req, res) => {
    const userId = parseInt(req.params.id);
    console.log(users[userId - 1].hasOrdered);
    if(!users[userId - 1].hasOrdered){
        throw new Error("Error. Nunca ha realizado un pedido, no puede modificar nada");
    } else if(ordersValidations.checkPendingStatus(userId)){
        order = orders.find(elem => elem.user.id === userId && elem.status === "Pendiente");
        const {dishName, paymentMethod, shippingAddress}  = req.body;
        let quantityProducts = req.body.quantityProducts;
        const detail = dishName.split(",");
        quantityProducts = quantityProducts.split(",");
        order.detail = detail;
        order.quantityProducts = quantityProducts;
        order.total = ordersValidations.calculateTotal(detail, quantityProducts);
        order.paymentMethod = paymentMethod;
        order.user.address = shippingAddress;
    }
    else {
        throw new Error("Su pedido ya fue cerrado")
    }
    
    res.json({message: `Orden modificada correctamente.`});
});

router.put("/confirmOrder/:id", inputsValidations.validateParamsInputs(usersValidations.isRegistered) 
                            , inputsValidations.validateBodyInputs(inputsValidations.isNumberId), (req, res) => {
    const userId = parseInt(req.params.id);
    let orderId = req.body.id;
    orderId--;
    console.log(userId);
    console.log(orderId);
    console.log(orders[orderId]);
    if(orderId >= orders.length)
        throw new Error("Error. No existe una orden con ese id");
    if(orders[orderId].user.id !== userId)
        throw new Error("Error. Id del usuario incorrecto/orden incorrecta");
    orders[orderId].status = "Confirmado";
    res.json({message: `Orden confirmada`});
});

router.delete("/delete", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), (req, res) => {
    res.send("Deleting orders data...");
});

module.exports = router;