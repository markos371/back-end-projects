const express = require("express");
const router = express.Router();
const paymentMethods = require("../models/paymentMethods");
const inputsValidations = require("../middlewares/inputsValidations")
const products = require("../models/products")
const paymentMethodsValidations = require("../middlewares/paymentMethodsValidations");

const balancePaymentMethodIndex = (data)=> {
    let paymentMethodId = parseInt(data.id);
    let paymentMethodsIndex = paymentMethodId - 1
    console.log("desde la funcion balance");
    console.log(paymentMethodId);
    console.log(paymentMethodsIndex);
    do{
        console.log(paymentMethods);
        console.log(paymentMethods[paymentMethodsIndex]);
        paymentMethods[paymentMethodsIndex].id = paymentMethodId++;  
        console.log(paymentMethods[paymentMethodsIndex]);   
        paymentMethodsIndex++; 
    }while(paymentMethodsIndex < paymentMethods.length)
};

router.get("/get", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), (req, res) => {
    res.json({message: paymentMethods});
});

// FALTA CREAR UN PAYMENTMETHODSVALIDATIONS PARA VALIDAR EL NUEVO MEDIO DE PAGO - modificar todo este codigo - 
router.post("/add", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), 
                inputsValidations.validateBodyInputs(paymentMethodsValidations.paymentMethod_validated), (req, res) => {
    let {paymentName} = req.body;
    const id = paymentMethods.length + 1;
    paymentMethods.push({id, paymentName});
    res.json({message: "Metodo de pago dado de alta"});
});

// ACA LO MIMSO QUE ARRIBA  - AMBOS COMENTADOS PARA LUEGO REVISARLOS
router.put("/update/:id", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), 
                   inputsValidations.validateBodyInputs(paymentMethodsValidations.paymentMethod_validated), (req, res) => {
    const paymentMethodId = req.params.id;
    const paymentMethodToUpdate = paymentMethods.find(elem => elem.id === parseInt(paymentMethodId));
    if(!paymentMethodToUpdate){
        throw new Error("No existe un metodo de pago con ese id");
    }
    const {paymentName} = req.body;
    paymentMethodToUpdate.paymentName = paymentName;
    res.status(200).json({message: `Metodo de pago actualizado`});
});

router.delete("/delete/:id", inputsValidations.validateHeadersInputs(inputsValidations.isAdmin), 
                      inputsValidations.validateParamsInputs(inputsValidations.isNumberId), (req, res) => {
    const paymentMethodId = req.params.id;
    const paymentMethodToDeleteIndex = paymentMethods.findIndex(elem => elem.id === parseInt(paymentMethodId));
    console.log(paymentMethodId);
    console.log(paymentMethodToDeleteIndex);
    if(paymentMethodToDeleteIndex === -1){
        throw new Error("No existe un metodo de pago con ese id");
    }
    paymentMethods.splice(paymentMethodToDeleteIndex, 1);
    if(paymentMethodToDeleteIndex !== paymentMethods.length)
        balancePaymentMethodIndex(req.params);
    console.log(paymentMethods);
    res.status(200).json({message:`Metodo de pago eliminado`});
});

module.exports = router;