const express = require("express");
const router = express.Router();
const users = require("../models/users");
const userValidations = require("../middlewares/usersValidations");

router.post("/", userValidations.validate(userValidations.newUser_validated), (req, res) => {
    let {userName, name_lastName, email, phoneNumber, shippingAddress, password} = req.body;
    const id = users.length + 1;
    const isAdmin = false;
    const isLogged = false;
    const hasOrdered = false;
    users.push({id, userName, name_lastName, email, phoneNumber, shippingAddress, password, isAdmin, isLogged, hasOrdered});
    res.status(200).json({message: `Registrado con exito, tu numero de id es ${id}`});
});

module.exports = router;