const express = require("express");
const router = express.Router();
const users = require("../models/users");
const usersValidations = require("../middlewares/usersValidations");

router.post("/", usersValidations.validate(usersValidations.user_validated), (req, res) => {
    const {userOrEmail, password} = req.body;
    const user = usersValidations.getUser(users, userOrEmail, password);
    user.isLogged = true;
    res.status(200).json({message:  `Se inicio sesion, su id es ${parseInt(user.id)}`});
});

module.exports = router;