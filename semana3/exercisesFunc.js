const valor = 4;

// Cuadrado
const cuadrado = num => num ** 2;
console.log(cuadrado(5));

// Factorial
const factorial = num => {
    let resultado = 1;
    if (Number.isInteger(num)){
        for(let i = 1; i <= num; i++){
            resultado *= i;
        }
        return resultado;
    }
    else return "No es un numero"
}
console.log(factorial(5));

// Radio al cuadrado
const circulo = radio => {
    const pi = 3.14159265358979;
    return pi * (radio ** 2);
}
console.log(circulo(2));

//Usuario y contraseña
const datosUsers = [['user1','password1'],['user2','password2'],['user3','password3'],['user4','password4']];
const verificarUsuario = (user,pasw) => {
    const largo = datosUsers.length;
    let esValido = false;
    for(i = 0; i < largo; i++){
        if(user === datosUsers[i][0] && pasw === datosUsers[i][1]){
            esValido = true;
            console.log(esValido);
            break;
        }
    }
    return esValido
}
console.log(verificarUsuario('user4','password4'));

/*document.getElementById("result1").innerHTML = console.log(`${valor} ** 2 = ${cuadrado(valor)}`);
document.getElementById("result2").innerHTML = console.log(`El factorial de ${valor} es: ${factorial(valor)}`);
document.getElementById("result3").innerHTML = console.log(`Area: ${circulo(valor)}`);
document.getElementById("result4").innerHTML = console.log(`Array: ${UserPass("Ale",1234)}`);*/
