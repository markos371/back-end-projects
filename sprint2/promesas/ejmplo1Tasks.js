const util = require('util');
// este modulo permite utilizar setTimeout con la funcion sleep
const  sleep = util.promisify(setTimeout);

module.exports = {

    async taskOne() {
        try {
            // throw new Error(`SOME PROBLEM`);
            await sleep(4000);
            return `One value`
        } catch(e) {
            console.log(e);
        }
    },

    async taskTwo() {
        try {
            await sleep(2000);
            return `Two value`
        } catch(e) {
            console.log(e);
        }
    }
};