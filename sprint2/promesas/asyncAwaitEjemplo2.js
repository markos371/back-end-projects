// CODIGO ASYNC-AWAIT EJECUTADO DE MANERA PARALELA

const {taskOne, taskTwo} = require('./ejmplo1Tasks');

async function main() {
    try {
        // esta funcion calcula el tiempo que tarda en ejecutrase las instrucciones
        console.time('Measuring time');
    

        // la siguiente linea me permite ejecutar todas las funciones que queremos ejeuctar paralelamente
        const results = await Promise.all([taskOne(), taskTwo()]) 
        // retorna un arreglo de los posibles valores que devuelvan estas funciones


        console.timeEnd("Measuring time");

        console.log(`Task one returned`, results[0]);
        console.log(`Task two returned`, results[1]);
    } catch(e) {
        console.log(e);
    }
}

main();