// CODIGO ASYNC-AWAIT EJECUTADO DE MANERA SECUENCIAL

const {taskOne, taskTwo} = require('./ejmplo1Tasks');

async function main() {
    try {
        // esta funcion calcula el tiempo que tarda en ejecutrase las instrucciones
        console.time('Measuring time');

        const valueOne = await taskOne();
        
        const valueTwo = await taskTwo();

        console.timeEnd("Measuring time");

        console.log(`Task one returned`, valueOne);
        console.log(`Task two returned`, valueTwo);
    } catch(e) {
        console.log(e);
    }
}

main();