const p1 = Promise.resolve(1);

console.log(p1);

// podemos devolver un valor o otra promesa
p1
.then(x => (x + 5))
.then(x => Promise.resolve(x + 5))
.then(x => Promise.reject(`Error! algo sucedio mal`))
// el siguiente then no se va a llamar porque anteriormente rechazamos con un error
// por lo tanto, va ignorar cualquier then posterior
// es neesario un catch para capturar el error
.then(x => console.log(`Esto no se va a llamar`))
.catch(e => console.log(e))


// funcion
const delayed = x => new Promise((resolve, reject) => setTimeout(() => resolve(x), 900))

delayed(7)
.then(x => {
    console.log(x)
    return delayed(x + 7)
})
.then(x => console.log(x))
.then(x => Promise.reject(`Hubo un error`))
.catch(e => console.log(e))