const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Acamica')
.then(() => console.log('Conectado a MongoDB...'))
.catch((err) => console.log('No se pudo conectar con MongoDB...', err));

const cursoSchema = new mongoose.Schema({
    nombre      : String, 
    autor       : String, 
    etiquetas   : [String],
    fecha       : {type: Date, default: Date.now},
    publicado   : Boolean
})

const Curso = mongoose.model("Curso", cursoSchema);

const crearCurso = async () => {
    const curso = new Curso({
        nombre: 'Python avanzado',
        autor: 'Facundo',
        etiquetas: ['Programacion en Python', 'Inteligencia Artificial'],
        publicado: true
    })

    const resultado = await curso.save();
    console.log(resultado);
};

// descomentar para crear un curso
// crearCurso();

const listarCursos = async () => {
    // devuelve todos los documentos de cursos
    const cursos = await Curso.find();
    // las demas formas de mostrar info esta en el otro archivo
    console.log(cursos);
};

//listarCursos();

const actualizarCurso1 = async (id) => {
    const curso = await Curso.findById(id);
    if(!curso) {
        console.log('El curso no existe');
        return 
    }

    curso.publicado = false;
    // estas son dos maneras de cambiar un valor a un atributo
    curso.set({
        autor: "Marcos Hinca"
    })

    const resultado = await curso.save();
    console.log(resultado);
};

// actualizarCurso1('617c2b7b210863c256ad73d5');

const actuzalizarCurso2 = async (id) => {
    // dado un id, modifico lo que quiero modificar con $set
    const resultado = await Curso.updateOne({_id: id}, {
        $set: {
            autor: 'Leo',
            publicado: false
        }
    })
    console.log(resultado);
};

// actuzalizarCurso2("617c2ae5ef21603f1c5c1754");

const actualizarCurso3 = async (id) => {
    const resultado = await Curso.findByIdAndUpdate(id, {
        $set: {
            nombre: "Python experto avanzado"
        }
    }, {new: true});
    console.log(resultado);
};
// ME MUESTRA LA INFO ANTES DE SER CAMBIADA
// SI QUIERO QUE MUESTRE EL VALOR AL SER CAMBIADO, SE ESCRIBE EL NUEVO PARAMETRO new:true
//actualizarCurso3("617c2c3a26241286dd5f2eb9");

const eliminarDocumento = async (id) => {
    const result = await Curso.deleteOne({_id: id})
    const resultado = await Curso.findByIdAndDelete(id);
    console.log(resultado);
};

eliminarDocumento('617c2c3a26241286dd5f2eb9');