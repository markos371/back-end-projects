const mongoose = require('mongoose');

const restaurantPlateSchema = new mongoose.Schema({
    plateName: {
        type: String,
        required: true
    },    
    price: {
        type: Number,
        required: true
    },
    plateType: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Plate', restaurantPlateSchema)