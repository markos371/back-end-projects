const express = require("express");
const mongoose = require('mongoose');
const config = require('./config/config');
const menu = require('./routes/menu')

mongoose.connect('mongodb://localhost:27017/Acamica')
.then(() => console.log('Conectado a MongoDB...'))
.catch(err => console.log('No se pudo conectar con MongoDB'))

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/api/menu", menu);

app.listen(config.port, () => console.log(`Api REST ejecutandose en el puerto ${config.port}`));
