const express = require('express');
const route = express.Router();
const Plate = require('../models/plate_model');

const crearPlate = async (body) => {
    const plate = new Plate({
        plateName: body.plateName,
        price: body.price,
        plateType: body.plateType
    })

    return await plate.save()
};

const listActivePlates = async () => {
    const plates = await Plate.find({"status": true});
    return plates
};

const updatePlate = async (plateName, body) => {
    let plate = await Plate.findOneAndUpdate({"plateName": plateName}, {
        $set: {
            plateName: body.plateName,
            price: body.price,
            plateType: body.plateType
        }
    }, {new: true});
    return plate
};

const desactivatePlate = async (plateName) => {
    let plate = await Plate.findOneAndUpdate({"plateName": plateName}, {
        $set: {
            status: false
        }
    }, {new: true});
    return plate
}

route.get('/get', (req, res) => {
    const result = listActivePlates();
    result.then(plates => {res.json({message: plates})})
    .catch(err => {
        res.status(400).json({message: err})
    });
});

route.post('/add', (req, res) => {
    const result = crearPlate(req.body)

    result.then(plate => {
        res.status(200).json({message: plate});
    }).catch(err => {
        res.status(400).json({message: err})
    });
});

route.put('/update/:plateName', (req, res) => {
    const result = updatePlate(req.params.plateName, req.body);

    result.then(value => {
        res.status(200).json({message: value})
    }).catch(err => {
        res.status(400).json({message: err})
    });
});

route.delete('/delete/:plateName', (req, res) => {
    const result = desactivatePlate(req.params.plateName);
    result.then(value => {
        res.status(200).json({message: value})
    }).catch(err => {
        res.status(400).json({message: err})
    });
});

module.exports = route;