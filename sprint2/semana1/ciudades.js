const cities= [
    {name: "Ushuaia"},
    {name: "Bogota"},
    {name: "New York"},
    {name: "Toronto"},
    {name: "Rio de Janeiro"},
    {name: "Milan"}, 
    {name: "Madrid"}, 
    {name: "Berlin"}, 
    {name: "Tokio"},
    {name: "Moscow"}
];

module.exports = cities;