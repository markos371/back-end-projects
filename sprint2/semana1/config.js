require('dotenv').config();

const config = {
    port: process.env.NODE_PORT || 5000,
    apiKey: process.env.API_KEY
}

module.exports = config