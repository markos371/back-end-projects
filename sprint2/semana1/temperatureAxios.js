const config = require("./config");
const axios = require("axios");
const express = require("express");
const app = express();
const cities = require("./ciudades");
const _ = require("lodash");

app.use(express.json());

const eliminarRepetidos = arr => [...new Set(arr)];

const threeCities = () => {
    let selectedCities = []
    do{
        selectedCities.splice(0, selectedCities.length);
        _.times(3, () => {
            const randomCity = randomCityName(cities);
            selectedCities.push(randomCity);
        })
        selectedCities = eliminarRepetidos(selectedCities)
    }while(selectedCities.length < 3)

    return selectedCities
}

function randomCityName(items) {
    return items[Math.floor((Math.random()*items.length))];
}

const clima_ciudad = async (ciudad) => {
    const baseApi = 'https://api.openweathermap.org';
    const apiKey = config.apiKey;
    try {
        const clima = await axios.get(`${baseApi}/data/2.5/weather?q=${ciudad}&appid=${apiKey}`)
        const dato = clima.data.main.temp;
        return dato;
    } catch(err) {
        console.log("Error", err);
    }
}

app.get("/clima", async (req, res) => {
    let cities = threeCities();
    try {
        for await(let city of cities)
            city.temperatura = await clima_ciudad(city.name);
        // cities[0].temperatura = await clima_ciudad(cities[0].name);
        // cities[1].temperatura = await clima_ciudad(cities[1].name);
        // cities[2].temperatura = await clima_ciudad(cities[2].name);
        res.status(200).send({"message": cities});
    } catch(err) {
        res.status(500).send({"message": err.message});
    }
});

app.listen(config.port, () => {
    console.log(`Escuchando en el puerto ${config.port}`);
});