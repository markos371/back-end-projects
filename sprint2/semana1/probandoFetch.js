require('isomorphic-fetch');

// con promesas
// fetch('https://jsonplaceholder.typicode.com/users')
// .then(x => x.json())
// .then(data => console.log(data))
// .catch(err => console.log("Error", err));

// con async y await - permite trabajar todo en el mismo hilo
const getUsuarios = async () => {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        const data = await response.json();
        console.log(data);
    } catch(err) {
        console.log("Error", err)
    }
    
};

getUsuarios();