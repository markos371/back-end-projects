const cities = require("./ciudades.js");
const express = require("express");
const app = express();
const axios = require("axios");
const _ = require("lodash");

app.use(express.json());

const eliminarRepetidos = arr => [...new Set(arr)];

function randomCityName(items) {
    return items[Math.floor((Math.random()*items.length))];
}

// ES ASI SOLO QUE PUEDO MOSTRAR LA TEMPERATURA Y DEVOLVER JUNTO AL NOMBRE DEL PAIS EN UN JSON
// AHORA SOLO DEVUELVE LA DATA DEL PRIMER ELEMENTO DEL ARRAY
app.get("/temperatura", (req, res) => {
    let newArrayCities = []
    do{
        newArrayCities.splice(0, newArrayCities.length);
        _.times(3, () => {
            const randomCity = randomCityName(cities);
            newArrayCities.push(randomCity);
        })
        newArrayCities = eliminarRepetidos(newArrayCities)
    }while(newArrayCities.length < 3)
    console.log(newArrayCities);
    axios({
        method: "GET",
        url: `https://api.openweathermap.org/data/2.5/weather?q=${newArrayCities[0]}&appid=8d607bf06c14052a2c1d804341705661`
    })
    .then(resp => {
        console.log(resp.data)
        res.json(resp.data.main)   
    })
    .catch(err => console.log(err))
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Escuchando en el puerto ${port}`);
});


// fetch("https://openweathermap.org/")
// .then(resp => resp.json())
// .then(data => console.log(data))
// .catch(err => console.log(err)) 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// function randomCityName(items) {
//     return items[Math.floor((Math.random()*items.length))];
// }

// app.get('/clima', (req, res) => {
//     let newArrayCitys = [];
//     const eliminarRepetidos = arr => [...new Set(arr)];
//     while (newArrayCitys.length < 3) {
//         let randomCity = randomCityName(citys);
//         newArrayCitys.push(randomCity);
//         newArrayCitys = eliminarRepetidos(newArrayCitys);
//     };
//     //puedo puedo iterar un axios.get?
//     axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[0].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
//         .then((respuesta) => {
//             console.log(respuesta.data);
//             newArrayCitys[0].temperatura = respuesta.data.main.temp;
//             axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[1].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
//                 .then((respuesta) => {
//                     newArrayCitys[1].temperatura = respuesta.data.main.temp;
//                     axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${newArrayCitys[2].name}&appid=e7ee0d2a1c3ca427bcd61f675ea5ce0c&units=metric`)
//                         .then((respuesta) => {
//                             newArrayCitys[2].temperatura = respuesta.data.main.temp;
//                             res.status(200).json({"response": newArrayCitys});
//                         })
//                 })
//         })
//         .catch((error) => {
//             console.log(error);
//             reject("ERROR");
//         })
        
        
// });