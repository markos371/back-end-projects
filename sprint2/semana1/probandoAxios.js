const axios = require("axios");

//CON AXIOS NO NECESITO USAR .JSON
// con promesas
// axios.get('https://jsonplaceholder.typicode.com/users')
// .then(response => console.log(response))
// .catch(err => console.log("Error", err))

// con async y AWAIT
// cuando tengo dos variables iguales puedo hacer que sea reconocida por otro nombre
const getUsuarios = async () => {
    const data = 'Marcos';
    const {data: datos} = await axios.get('https://jsonplaceholder.typicode.com/users');
    console.log(datos);
}

getUsuarios();
