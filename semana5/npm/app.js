const coolImages = require("cool-images");
const fs = require("fs");
const moment = require("moment");
moment.locale("es");

const urlImages = coolImages.many(600, 800, 10);

urlImages.forEach(url => {
    fs.appendFile("url_images.txt", `\nURL: ${url} - Guarda el: ${moment().format("llll")}`, (error) => {
        if(error){
            console.log(error);
        } else {
            console.log("Se guardo correctamente el url de la imagen");
        }
    })
})

//ERRORES ARRIBA