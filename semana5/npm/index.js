const env = require("./env.example.json");
const chalk = require("chalk");
const moment = require("moment");
// const unique = require("unique"); // devuelve una lista con todos los elementos sin repetir(unicos).

const node_env = process.env.NODE_ENV || "persona";

const variables = env[node_env];

// challenge npm 
console.log(`
Hora actual: ${moment(new Date()).format("ll")}
Persona: ${chalk.green(variables.nombre)} ${chalk.underline.green(variables.apellido)}, ${chalk.red(variables.edad)}`);