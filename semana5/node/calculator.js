const mostrar = mensaje => {
    console.log(mensaje);
}

const sumar = (num1, num2) => `${num1} + ${num2} = ${num1 + num2}`;
const restar = (num1, num2) => `${num1} - ${num2} = ${num1 - num2}`;
const multiplicar = (num1, num2) => `${num1} * ${num2} = ${num1 * num2}`;
const dividir = (num1, num2) => {
    if(num2 !== 0)
        return `${num1} / ${num2} = ${num1 / num2}`
    else 
        return `No es posible deivir por 0 (cero)`
}

module.exports = {
    mostrar,
    sumar,
    restar,
    multiplicar,
    dividir
}
