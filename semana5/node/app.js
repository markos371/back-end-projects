const fs = require("fs");
const calculadora = require("./calculator");

// fs.readdir("./", (err, files) => {
//     files.forEach(file => console.log(file));
// })
// console.log(fs.readdirSync("./"));

const ops = [];
conjunto = "";

const op1 = (calculadora.sumar(6, 5));
ops.push(op1 + "\n");
const op2 = (calculadora.restar(5, 4));
ops.push(op2 + "\n");
const op3 = (calculadora.multiplicar(5, 5));
ops.push(op3 + "\n");
const op4 = (calculadora.dividir(100, 10));
ops.push(op4 + "\n");

for(let op of ops){
    conjunto += `${op}\t`;
}


fs.writeFile("log.txt",conjunto, err => {
    if(err) 
        console.log("Error al crear el archivo");
    else{
        console.log("El archivo fue creado exitosamente!");
    }
})