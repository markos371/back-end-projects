console.log("Hola Mundo desde node.js")

const mascotas = ["perro", "gato", "pez", "pajaro"];

const mostrar = (mascota, id) => console.log(id + " - " + mascota);
const mostrarMascotas = (listadoMascotas) => listadoMascotas.forEach(mostrar);

mostrarMascotas(mascotas);